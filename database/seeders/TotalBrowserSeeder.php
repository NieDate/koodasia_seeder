<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\TotalBrowser;

class TotalBrowserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $startDate = '2022-12-01';
        $currencies = ['MYR', 'SGD', 'IDR', 'PHP', 'VND'];
        $browsers = ['Chrome', 'Firefox', 'Safari', 'UNKNOWN'];

        while (strtotime($startDate) <= strtotime('2023-01-31')) {
            foreach($browsers as $browser){
                TotalBrowser::create([
                    'date' => $startDate,
                    'count' => $faker->numberBetween(1, 100),
                    'suspended' => $faker->numberBetween(1, 100),
                    'sales' => $faker->numberBetween(1, 100),
                    'currency' => $currencies[$faker->numberBetween(0, 4)],
                    'browser_id' => $faker->numberBetween(1, 100),
                    'browser_name' => $browser,
                ]);
            }

            $startDate = date('Y-m-d', strtotime('+1 day', strtotime($startDate)));
        }
    }
}
