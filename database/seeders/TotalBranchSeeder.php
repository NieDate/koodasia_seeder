<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\TotalBranch;

class TotalBranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $startDate = '2022-12-01';
        $currencies = ['MYR', 'SGD', 'IDR', 'PHP', 'VND'];
        $branches = ['Test Branch', 'Test Branch #2', 'Test Country'];
        $branch_id = [58, 59, 74];
        while (strtotime($startDate) <= strtotime('2023-01-31')) {
            $i = 0;
            foreach($branches as $branch){
                TotalBranch::create([
                    'date' => $startDate,
                    'count' => $faker->numberBetween(1, 100),
                    'suspended' => $faker->numberBetween(1, 100),
                    'sales' => $faker->numberBetween(1, 100),
                    'currency' => $currencies[$faker->numberBetween(0, 4)],
                    'branch_id' => $branch_id[$i],
                    'branch_name' => $branch,
                ]);
                $i++;
            }

            $startDate = date('Y-m-d', strtotime('+1 day', strtotime($startDate)));
        }
    }
}
