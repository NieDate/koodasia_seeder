<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\TotalPlatform;

class TotalPlatformSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $startDate = '2022-12-01';
        $currencies = ['MYR', 'SGD', 'IDR', 'PHP', 'VND'];
        $platforms = ['Android', 'IOS', 'UNKNOWN'];

        while (strtotime($startDate) <= strtotime('2023-01-31')) {
            foreach($platforms as $platform){
                TotalPlatform::create([
                    'date' => $startDate,
                    'count' => $faker->numberBetween(1, 100),
                    'suspended' => $faker->numberBetween(1, 100),
                    'sales' => $faker->numberBetween(1, 100),
                    'currency' => $currencies[$faker->numberBetween(0, 4)],
                    'platform_id' => $faker->numberBetween(1, 100),
                    'platform_name' => $platform,
                ]);
            }

            $startDate = date('Y-m-d', strtotime('+1 day', strtotime($startDate)));
        }
        
    }
}
