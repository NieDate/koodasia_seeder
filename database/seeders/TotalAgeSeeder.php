<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\TotalAge;

class TotalAgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $startDate = '2022-12-01';
        $currencies = ['MYR', 'SGD', 'IDR', 'PHP', 'VND'];
        $age_groups = ['17-', '18-24', '25-34', '35-44', '45-54', '55-64', '65+', 'UNKNOWN'];

        while (strtotime($startDate) <= strtotime('2023-01-31')) {
            foreach($age_groups as $age_group){
                TotalAge::create([
                    'date' => $startDate,
                    'count' => $faker->numberBetween(1, 100),
                    'suspended' => $faker->numberBetween(1, 100),
                    'sales' => $faker->numberBetween(1, 100),
                    'currency' => $currencies[$faker->numberBetween(0, 4)],
                    'age_group_id' => $faker->numberBetween(1, 100),
                    'age_group_name' => $age_group,
                ]);
            }

            $startDate = date('Y-m-d', strtotime('+1 day', strtotime($startDate)));
        }
    }
}
