<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalPlatform extends Model
{
    use HasFactory;
    protected $table = 'total_platform_37';

    protected $fillable = [
        'date',
        'count',
        'suspended',
        'sales',
        'currency',
        'platform_id',
        'platform_name'
    ];
}
