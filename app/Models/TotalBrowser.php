<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalBrowser extends Model
{
    use HasFactory;
    protected $table = 'total_browser_37';

    protected $fillable = [
        'date',
        'count',
        'suspended',
        'sales',
        'currency',
        'browser_id',
        'browser_name'
    ];
}
