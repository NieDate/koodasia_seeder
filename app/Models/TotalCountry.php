<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalCountry extends Model
{
    use HasFactory;

    protected $table = 'total_by_country_37';

    protected $fillable = [
        'date',
        'count',
        'suspended',
        'sales',
        'currency',
        'country',
    ];
}
