<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalScan extends Model
{
    use HasFactory;

    protected $table = 'total_scan_37';

    protected $fillable = [
        'date',
        'count',
        'suspended',
        'sales',
        'currency'
    ];
}
