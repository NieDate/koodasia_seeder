<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalBranch extends Model
{
    use HasFactory;
    protected $table = 'total_branch_37';

    protected $fillable = [
        'date',
        'count',
        'suspended',
        'sales',
        'currency',
        'branch_id',
        'branch_name'
    ];
}
