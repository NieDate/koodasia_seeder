<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalCampaign extends Model
{
    use HasFactory;
    protected $table = 'total_campaign_37';

    protected $fillable = [
        'date',
        'count',
        'suspended',
        'sales',
        'currency',
        'campaign_id',
        'campaign_name'
    ];
}
