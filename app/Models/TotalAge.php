<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalAge extends Model
{
    use HasFactory;
    protected $table = 'total_age_37';

    protected $fillable = [
        'date',
        'count',
        'suspended',
        'sales',
        'currency',
        'age_group_id',
        'age_group_name'
    ];
}
